﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Senseforce.API.Hubs;

namespace Senseforce.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MessageTestController : ControllerBase
    {
        private readonly IHubContext<MessageHub> _hubContext;

        public MessageTestController(IHubContext<MessageHub> hubContext)
        {
            _hubContext = hubContext ?? throw new ArgumentNullException(nameof(hubContext));
        }

        [HttpPost]
        public async Task<IActionResult> Post(string message)
        {
            await _hubContext.Clients.All.SendAsync("Message", message);
            return Ok();
        }
    }
}