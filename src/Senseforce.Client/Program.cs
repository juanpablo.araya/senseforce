﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Senseforce.Client.HostedServices;
using Senseforce.Client.Services;
using Senseforce.Client.Services.Abstractions;

namespace Senseforce.Client
{
    class Program
    {
        static async Task Main(string[] args)
        {
            await Host.CreateDefaultBuilder(args)
                .ConfigureServices((context, services) =>
                {
                    services.AddHostedService<MessageConsumerHostedService>();
                    services.AddHostedService<QueuedMessageHostedService>();
                    services.AddSingleton<IBackgroundMessageQueue, BackgroundMessageQueue>();
                    services.AddSingleton(GetSignalRConnection(context.Configuration));
                    services.AddSingleton<ICommandGenerator, CommandGenerator>();
                    AddRepository(context.Configuration, services);
                    services.AddSingleton<IMessageProcessor, MessageProcessor>();
                    services.AddSingleton(new Configuration {ConnectionString = context.Configuration.GetConnectionString("Senseforce")});
                })
                .RunConsoleAsync();

        }

        private static void AddRepository(IConfiguration configuration, IServiceCollection services)
        {
            if (configuration["EmptyRepository"] == "True")
            {
                services.AddSingleton<ICommandRepository, EmptyRepository>();
            }
            else
            {
                services.AddSingleton<ICommandRepository, CommandRepository>();
            }
        }

        private static HubConnection GetSignalRConnection(IConfiguration configuration)
        {
            return new HubConnectionBuilder()
                .WithUrl(configuration["SignalRHub"])
                .Build();
        }
    }
}