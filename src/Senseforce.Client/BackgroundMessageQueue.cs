﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;

namespace Senseforce.Client
{
    public interface IBackgroundMessageQueue
    {
        void QueueMessage(string message);

        Task<JObject> DequeueAsync(CancellationToken cancellationToken);
    }
 
    /// <summary>
    /// Queue class. Receives a <see cref="string"/> as message. Tries to parse it and enqueue it as a valid <see cref="JObject"/> object.
    /// </summary>
    public class BackgroundMessageQueue: IBackgroundMessageQueue
    {
        private readonly ILogger<BackgroundMessageQueue> _logger;
        private readonly ConcurrentQueue<JObject> _messages  = new();
        private readonly SemaphoreSlim _signal = new(0);

        public BackgroundMessageQueue(ILogger<BackgroundMessageQueue> logger)
        {
            _logger = logger;
        }
        
        public void QueueMessage(string message)
        {
            if (message == null)
            {
                throw new ArgumentNullException(nameof(message));
            }

            try
            {
                var json = JObject.Parse(message);
                _messages.Enqueue(json);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Cannot enqueue message {message}");
                throw;
            }

            _signal.Release();
        }
        
        public async Task<JObject> DequeueAsync(
            CancellationToken cancellationToken)
        {
            await _signal.WaitAsync(cancellationToken);
            _messages.TryDequeue(out var message);

            return message;
        }
    }
}