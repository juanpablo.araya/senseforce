﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Senseforce.Client.HostedServices
{
    /// <summary>
    /// Enqueues messages received from SignalR to a <see cref="IBackgroundMessageQueue"/>
    /// </summary>
    internal class MessageConsumerHostedService: IHostedService, IAsyncDisposable
    {
        private readonly ILogger<MessageConsumerHostedService> _logger;
        private readonly IHostApplicationLifetime _applicationLifetime;
        private readonly HubConnection _hubConnection;
        private readonly IBackgroundMessageQueue _backgroundMessageQueue;

        public MessageConsumerHostedService(
            ILogger<MessageConsumerHostedService> logger,
            IHostApplicationLifetime applicationLifetime,
            HubConnection hubConnection,
            IBackgroundMessageQueue backgroundMessageQueue)
        {
            _logger = logger;
            _applicationLifetime = applicationLifetime;
            _hubConnection = hubConnection;
            _backgroundMessageQueue = backgroundMessageQueue;
        }
        
        public Task StartAsync(CancellationToken cancellationToken)
        {
            _applicationLifetime.ApplicationStarted.Register(() =>
            {
                Task.Run(async () =>
                {
                    try
                    {
                        _logger.LogDebug("Starting SignalR connection");
                        _hubConnection.On<string>("Message", ReceiveMessage);
                        await _hubConnection.StartAsync(cancellationToken);
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError(ex, "Unhandled exception");
                    }
                }, cancellationToken);
            });
            
            return Task.CompletedTask;
        }

        private void ReceiveMessage(string message)
        {
            _logger.LogInformation($"Received message {message}");
            _backgroundMessageQueue.QueueMessage(message);
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            await _hubConnection.StopAsync(cancellationToken);
        }

        public async ValueTask DisposeAsync()
        {
            await _hubConnection.DisposeAsync();
        }
    }
}