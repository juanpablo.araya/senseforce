﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Senseforce.Client.Services.Abstractions;

namespace Senseforce.Client.HostedServices
{
    /// <summary>
    /// Receives the messages from the <see cref="IBackgroundMessageQueue"/> to process them  
    /// </summary>
    public class QueuedMessageHostedService: BackgroundService
    {
        private readonly IBackgroundMessageQueue _messageQueue;
        private readonly ILogger<QueuedMessageHostedService> _logger;
        private readonly IMessageProcessor _messageProcessor;

        public QueuedMessageHostedService(
            IBackgroundMessageQueue messageQueue,
            ILogger<QueuedMessageHostedService> logger,
            IMessageProcessor messageProcessor)
        {
            _messageQueue = messageQueue;
            _logger = logger;
            _messageProcessor = messageProcessor;
        }
        
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                string messageId = null;
                try
                {
                    var message = await _messageQueue.DequeueAsync(stoppingToken);
                    messageId = message["id"].ToString();
                    await _messageProcessor.Process(message, stoppingToken);
                    _logger.LogInformation($"Processed message with id {messageId}.");
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"Cannot process the following message: {messageId}");
                }
            }
        }
    }
}