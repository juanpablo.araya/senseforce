﻿using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Senseforce.Client.Services.Abstractions;

namespace Senseforce.Client.Services
{
    public class MessageProcessor: IMessageProcessor
    {
        private readonly ICommandGenerator _commandGenerator;
        private readonly ICommandRepository _commandRepository;
        
        public MessageProcessor(
            ICommandGenerator commandGenerator,
            ICommandRepository commandRepository)
        {
            _commandGenerator = commandGenerator;
            _commandRepository = commandRepository;
        }
        
        public async Task Process(JObject message, CancellationToken cancellationToken)
        {
            var command = _commandGenerator.Get(message);
            await _commandRepository.Save(command, cancellationToken);
        }
    }
}