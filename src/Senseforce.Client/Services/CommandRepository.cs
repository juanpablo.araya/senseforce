﻿using System.Threading;
using System.Threading.Tasks;
using Dapper;
using Npgsql;
using Senseforce.Client.Services.Abstractions;

namespace Senseforce.Client.Services
{
    /// <summary>
    /// Repository. Inserts the command into a PostgreSQL database
    /// </summary>
    public class CommandRepository: ICommandRepository
    {
        private readonly Configuration _configuration;

        public CommandRepository(Configuration configuration)
        {
            _configuration = configuration;
        }
        
        public async Task Save(string command, CancellationToken cancellationToken)
        {
            await using var connection = new NpgsqlConnection(_configuration.ConnectionString);
            await connection.OpenAsync(cancellationToken);
            await connection.ExecuteAsync(command, cancellationToken);
        }
    }
}