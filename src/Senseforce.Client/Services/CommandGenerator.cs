﻿using System.Text;
using Newtonsoft.Json.Linq;
using Senseforce.Client.Services.Abstractions;

namespace Senseforce.Client.Services
{
    public class CommandGenerator: ICommandGenerator
    {
        /// <summary>
        /// Generates a SQL instruction, based on the <see cref="JObject"/> received
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public string Get(JObject message)
        {
            var sb = new StringBuilder($"INSERT INTO {message["type"]}(timestamp, id");

            var dataProperties = (message["data"] as JObject).Properties();
            foreach (var property in dataProperties)
            {
                if(property.Value.Type == JTokenType.Null)
                    continue;
                sb.Append($", {property.Name}");
            }
            sb.Append($") VALUES({message["timestamp"]}, '{message["id"]}'");

            foreach (var property in dataProperties)
            {
                if(property.Value.Type == JTokenType.Null)
                    continue;

                var value = property.Value;
                if(property.Value.Type == JTokenType.String)
                    value = $"'{value}'";
                
                sb.Append($", {value}");
            }

            sb.Append(");");

            return sb.ToString();
        }
    }
}