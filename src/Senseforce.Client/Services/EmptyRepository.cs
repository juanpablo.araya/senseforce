﻿using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Senseforce.Client.Services.Abstractions;

namespace Senseforce.Client.Services
{
    /// <summary>
    /// Empty repository. Only logs the SQL command. Does not do any DB operation
    /// </summary>
    public class EmptyRepository: ICommandRepository
    {
        private readonly ILogger<EmptyRepository> _logger;

        public EmptyRepository(ILogger<EmptyRepository> logger)
        {
            _logger = logger;
        }
        
        public Task Save(string command, CancellationToken cancellationToken)
        {
            _logger.LogInformation(command);
            return Task.CompletedTask;
        }
    }
}