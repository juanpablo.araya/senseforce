﻿using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace Senseforce.Client.Services.Abstractions
{
    public interface IMessageProcessor
    {
        Task Process(JObject message, CancellationToken cancellationToken);
    }
}