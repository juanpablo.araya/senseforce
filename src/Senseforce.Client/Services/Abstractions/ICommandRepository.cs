﻿using System.Threading;
using System.Threading.Tasks;

namespace Senseforce.Client.Services.Abstractions
{
    public interface ICommandRepository
    {
        Task Save(string command, CancellationToken cancellationToken);
    }
}