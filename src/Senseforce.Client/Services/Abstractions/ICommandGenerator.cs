﻿using Newtonsoft.Json.Linq;

namespace Senseforce.Client.Services.Abstractions
{
    public interface ICommandGenerator
    {
        string Get(JObject message);
    }
}