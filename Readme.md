# Coding Puzzle SignalR

The task requires to create a SignalR client console app in .Net core. The hub sends JSON messages 
with information about table and sensor values. The client should insert those values in the respective 
table. The following additional conditions should be met:

1. Implement a stop method to shut down your client.
2. Make sure your implementation is stateless.
3. Your code should be able to process a reasonable amount of messages per second.
4. Provide a superficial test-concept for how to test the implementation.

# Solution

The solution contains two projects:

1. **Client**: the solution (more on that later)
2. **API**: used to test the signalR messages.

### API
The _API_ project just contains a simple controller. When running it, it shows the swagger tool, where you can 
execute and send messages via SignalR. The controller just receives the message (as string) and sends it 
to all the connected clients.

### Client

The client is a console application that has two hosted services:

1. **MessageConsumerHostedService**: This is the service that is connected to SignalR. Here we 
do not process the messages. Instead, we enqueue them into a concurrent queue. The reason for that is to be able
   to receive the messages as they arrive, as the processing is the most time consuming process. 
2. **QueuedMessageHostedService**: This service is constantly dequeuing and processing the messages. For the moment, it only 
processes one message at a time. The idea is that the signalR messages arrive and are queued, and this services starts to process them. If the amount of messages per second are faster than 
   the processing time per message, they will be kept in the queue, till the processor can cope with the messages.
   
### Client: services and abstractions

1. **MessageProcessor**: used by **QueuedMessageHostedService**. Basically, receives the 
JSON object, transforms the data into a SQL instruction, and then call the repository to store the values into the database.
2. **CommandGenerator**: takes the JSON data and generates a simple SQL insert command. Returns a string.
4. **CommandRepository** and **EmptyRepository**: _CommandRepository_ inserts the command into a PostgreSQL database. Empty repository 
only shows the generated SQL instruction. Used for testing the process without a real PostgreSQL database. This value can be
   changed in _appsettings.json_. The property value is **EmptyRepository**.
   
### Client: configurations

The _appsettings.json_ file only has three main configurations:

- _SignalRHub_: URL of the SignalR hub
- _ConnectionStrings:Senseforce_: ConnectionString of the PostgreSQL database
- _EmptyRepository_: used to inject the EmptyRepository (true) or the CommandRepository (false/other value)

## About the conditions to be met:

1. _Implement a stop method to shut down your client._: the console application uses Generic Host. By pressing _Control + C_ the application will shut down. 
2. _Make sure your implementation is stateless_: there are no states when the app starts/stop. We just start connecting to the hub, processing the messages, after being enqueued. 
3. _Your code should be able to process a reasonable amount of messages per second_: for this, the solution that I chose was to enqueue the messages. If at some point the 
   hub starts to send several messages, the higher cost is the round-trip to the database. That's why we enqueue them first. Also, we use extensive 
   logging (by default, saving to console), that allows us to recover from errors, in case a message is not processed. 
4. _Provide a superficial test-concept for how to test the implementation_:

Most of the implementation use abstractions. I tried to separate responsibilities into different interfaces, to simplify unit testing. The most sensitive
codes are in the _CommnadGenerator_ and the _CommandRepository_. 

#### Which king of tests yo suggest for a Continuous integration environment

In particular, the repository depends on a
PostgreSQL database (I don't have one, so I didn't test the inserts). The idea is to have integration tests to verify the data
is inserted correctly. For that we can

- Initialize an empty database, creating the schemas
- Configure injection for _MessageProcessor_, _CommandRepository_ and _CommandGenerator_
- Prepare a set of JSON objects (strings) and call the _MessageProcessor.Process_ method, passing the string parsed 
as JSON objects.
- Validate the data is correctly inserted.

For the _CommandGenerator_ the same: a set of messages, and validate the generated SQL.

This is a test at the client level. To test the integration with SignalR, the process is similar:

- Prepare an environment with the client and the Hub
- Define a set of postman scripts. Execute them against the API method (or manually via swagger). 
- Check that the data was processed correctly in the database, after sending the set of messages.

#### Would you mock the external components like SignalR hub (server) and PostgreSQL database? If so, describe your mocking strategy. If not, describe why.

In terms of integration tests, I would not test integration with mocked services. I would mock them only in terms of unit tests.
I think the biggest challenge in this exercise is mainly the connection between the different services (signalR, PostgreSQL). Especially under 
stress conditions. That's why I don't recommend to mock.

If we talk about unit tests and validating the codes at the atomic level, it's fine with mocking, depending what you are testing. If you are interested
in how your services are implemented, or how to improve/refactor the codes, I would mock, but also rely on the integration tests at the end.

#### Shortly describe, which testing and mocking frameworks you are used to.

For unit tests: basically xUnit and Moq. NUnit too. Additional tools: 

- Shouldly as extension methods to make the assertions easier to read
- Automock for auto mocking methods with large dependencies.

About integration tests: a bit of selenium and postman. I do have more experience with unit tests, as in most of my projects we had a dedicated
team of manual and automation testers.

## Improvements

Some of the improvements I would like to include are:

1. Simplify the _commandGenerator_, or get rid of it. I thought at the beginning about using Entity Framework and strongly typed classes, 
but I needed to know upfront the columns per each table/class. That's the problem with the EF approach. However, I still think this command generator is very prone to errors. Perhaps with more case scenarios, and
   a real database I can tweak it and see if the SQL generation is the best approach or not.
2. Process in bulks: the queue approach only process one message at a time. If we decide to go with a SQL generation approach, better to send the insert in bulks. With the current implementation, we can send in bulks of 1000s 
to increase the performance. Also, I read that PostgreSQL has the copy command. In that case, maybe we can
   generate a file and execute only one command. In that case the approach should change a bit, as we need to group the copy per table.
3. Include resilience to the connections: right now, the code does not verify when the connection is lost, nor has a way to reconnect, or recover from errors. We need to add this feature to recover from network errors, exceptions, etc.
4. Make sure all the messages are processed: right now, if the console app is closed while the queue have messages, all those messages are lost. We need to store them before closing, to make sure
no message gets lost.
   